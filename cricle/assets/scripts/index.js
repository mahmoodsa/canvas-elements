let canvas = document.querySelector('canvas');

canvas.height = window.innerHeight; 
canvas.width = window.innerWidth;

let c = canvas.getContext('2d');

var colorArray = [
    '#2c3e50',
    '#e74c3c',
    '#ecf0f1',
    '#3498db',
    '#298089',
    '#F2762E'
]
var mouse = {
    x:undefined,
    y:undefined
}

var maxRadius = 40;
var minRadius = 2;

window.addEventListener('mousemove' , function(e) {
    mouse.x = e.x;
    mouse.y = e.y;
})

window.addEventListener('resize' , function(e) {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    init();
})
var circleArray = [];
function init() {
    circleArray = [];
    for(var i = 0 ; i < 1000 ; i++ ) {
        let radius = Math.random() * 4 + 1;
        let x = Math.random() * (innerWidth - 2*radius) + radius;
        let y = Math.random() * (innerHeight - 2*radius) + radius;
        let dx = (Math.random() - .5) * 2;
        let dy = (Math.random() - .5) * 2;
        circleArray.push(new Circle(x,y,dx,dy,radius));
    }
}
init();
function animate() {
    requestAnimationFrame(animate);
    c.clearRect(0,0,innerWidth, innerHeight)
    circleArray.forEach(item => {
        item.update()
    })
}

function Circle(x, y , dx, dy, radius) {
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.radius = radius;
    this.color = colorArray[Math.floor(Math.random() * colorArray.length)]
    this.minRadius = radius;

    this.draw = function() {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, Math.PI * 2 , false)    
        c.fillStyle = this.color
        c.fill();
    }
    
    this.update = function() {
        if(this.x + this.radius > innerWidth || this.x - this.radius < 0) {
            this.dx = -this.dx
        }
        if(this.y + this.radius > innerHeight || this.y - this.radius < 0) {
            this.dy = -this.dy
        }
        this.x += this.dx
        this.y += this.dy

        if(mouse.x-this.x<  50 
            && mouse.x - this.x > -50
            && mouse.y - this.y < 50
            && mouse.y - this.y > -50 ) {
                if(this.radius < maxRadius) {
                    this.radius +=3
                }
        } else if(this.radius > this.minRadius + 1) {
            this.radius -=3;
        }

        this.draw();
    }
}


animate()

// var gl = canvas.getContext('webgl')
// console.log(gl);
// if(!gl) {
//     console.log('WebGL not supported!');
//     gl = canvas.msGetRegionContent('experimental-webgl')
// }
// if(!gl) {
//     alert('WebGL not supported!');
// }

// canvas.height = window.innerHeight; 
// canvas.width = window.innerWidth;
// gl.viewport(0, 0, window.innerWidth, window.innerHeight);
// gl.clearColor(0,0,0,1);
// gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


// function vertexShader(vertPosition, vertColor) {
//     return {
//         fragColor: vertColor,
//         gl_position: [vertPosition.x, vertShader.y , 0.0, 1.0];
//     }
// }