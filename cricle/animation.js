let HW = window.innerWidth / 2;
let HH = window.innerHeight / 2;
let timeout;
function update(e) {
    positionX = e.clientX - HW
    positionY = e.clientY - HH
    translateX = positionX / 9
    translateY = positionY / 9
    rotateX = positionY / 15
    rotateY = -positionX / 15
    return () => {document.querySelector('.card').style.transform = `perspective(500px) rotateX(${rotateX}deg) rotateY(${rotateY}deg) scale3d(1.1,1.1,1.1)`}
    
}
function setTransition() {
    document.querySelector('.card').style.transition = `all 400ms cubic-bezier(0.03, 0.98, 0.52, 0.99) 0s`;
    document.querySelector('.card').style.willChange = `transform`
    setTimeout(() => {
        document.querySelector('.card').style.transition = ``;
    }, 400);
}
function reset() {
    return () => {document.querySelector('.card').style.transform = `perspective(500px) rotateX(0deg) rotateY(0deg) scale3d(1,1,1)`}
}

document.querySelector('.card').addEventListener('mousemove', function(e) {
    cancelAnimationFrame(update(e))
    requestAnimationFrame(update(e))
})
document.querySelector('.card').addEventListener('mouseenter', function(e) {
    setTransition();
})

document.querySelector('.card').addEventListener('mouseleave' , function() { 
    setTransition();
    requestAnimationFrame(reset())
})